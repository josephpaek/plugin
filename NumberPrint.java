public class NumberPrint {

	static int num = 0;

	public static void main(String[] args) {

		startCountTo10(num);
		startCountTo100(num);
		startCountTo1000(num);
	}

	private static void startCountTo10(int n) {

		System.out.println("Starting count from " + num);
		for (int i = n; i < 10; i++) {
			System.out.println(i);
			num += 1;
		}
	}

	private static void startCountTo100(int n) {

		System.out.println("Starting count from " + n);
		for (int i = n; i < 100; i++) {
			System.out.println(i);
			num += 1;
		}
	}

	private static void startCountTo1000(int n) {

		System.out.println("Starting count from " + n);
		for (int i = n; i <= 1000; i+=10) {
			System.out.println(i);
			num += 10;
		}
	}
}
